
package com.sikuliTest.spotify;

import org.sikuli.script.FindFailed;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.sikuliTest.util.DataProperties;


public class SearchAndPlayTest extends ScriptBase {
	

	@Test
	public void searchAndPlayTest() throws FindFailed, InterruptedException {
		spotifyApp().homepage().verifyPlayWindow();
		spotifyApp().homepage().getSongSearch(DataProperties.get("search.request"));
		spotifyApp().homepage().clickArtistSong();
		spotifyApp().homepage().getSongPlayButtons();
		Assert.assertTrue(spotifyApp().homepage().verifySongPlaying());

	}

}
