package com.sikuliTest.spotify;

import static org.testng.Assert.fail;

import java.io.IOException;

import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import com.sikuliTest.pom.SpotifyApplication;
import com.sikuliTest.util.DataProperties;

public class ScriptBase {

	protected App spotApp;
	protected SpotifyApplication spotifyApp;
	protected Screen page;
	protected Process app;

	@BeforeMethod
	public void setUp() throws InterruptedException, FindFailed {
		app = run();
	}

	@AfterMethod
	public void tearDown() {
		stop();
	}

	private Process run() {
		try {
			return Runtime.getRuntime().exec(DataProperties.get("spotify.path"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void stop() {
		app.destroy();
	}

	private void openSpotify() throws InterruptedException, FindFailed {
		try {
			spotApp = new App(DataProperties.get("spotify.path"));
		} catch (Exception e) {
			fail("Can't opeen Spotify by path " + DataProperties.get("spotify.path"));
		}
	}

	private void closeSpotify() {
		spotApp.close("Spotify");
	}

	public SpotifyApplication spotifyApp() {
		if (spotifyApp == null) {
			spotifyApp = new SpotifyApplication(page);
		}
		return spotifyApp;
	}
}
