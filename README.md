# README #

It is a simple automation test framework for Sikuli image based testing. for this project it has been used to automate spotify desktop application with 
Java and maven as a build tool. 

### Installation Instruction ### 

1.In you Maven project POM.xml add the following dependency for Sikuli.
   1.     <dependency>
			<groupId>com.sikulix</groupId>
			<artifactId>sikulixapi</artifactId>
			<version>1.1.2-SNAPSHOT</version>
		  </dependency>
2.Add a repository for Sikuli. github link is given below.
   1.     https://github.com/RaiMan/SikuliX-2014.
  
          <repository>
			<id>com.sikulix</id>
			<name>com.sikulix</name>
			<url>https://oss.sonatype.org/content/groups/public</url>
			<layout>default</layout>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
			</snapshots>
          </repository>
		  
3.If using Eclips IDE. make sure you have TestNG plugin added to run test. 
4.Update Maven Project with added dependency in pom.xml and start running the test. 
	
