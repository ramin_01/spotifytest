package com.sikuliTest.pom;

import org.sikuli.script.Screen;

public class Page {
	
protected static Screen page;

public Page(Screen page){
	Page.page = page;
}
	
	public synchronized static Screen getInstance() {
		if (page == null){
			page = new Screen();
		}
		return page;
	}
	
	public Screen getPage() {
		return Page.getInstance();
	}
}
