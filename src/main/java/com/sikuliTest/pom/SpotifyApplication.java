package com.sikuliTest.pom;

import org.sikuli.script.Screen;
import com.sikuliTest.pom.Page;

public class SpotifyApplication extends Page {

	protected LoginPage loginPage;
	protected HomePage homepage;

	public SpotifyApplication(Screen page) {
		super(page);

	}

	public HomePage homepage() {
		if (homepage == null) {
			homepage = new HomePage(page);
		}
		return homepage;
	}

	public LoginPage loginPage() {
		if (loginPage == null) {
			loginPage = new LoginPage(page);
		}
		return loginPage;
	}

}
