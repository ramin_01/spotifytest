package com.sikuliTest.pom;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.sikuliTest.util.DataProperties;

public class HomePage extends Page {

	public HomePage(Screen page) {
		super(page);
	}

	private Pattern songSearch;
	private Pattern artistSongPlay;
	private Pattern songPlayButtons;
	private Pattern songPauseButton;
	private Pattern playWindow;

	private Region window;

	public Pattern getSongSearch(String artist) throws FindFailed {
		songSearch = new Pattern(DataProperties.path("songSearchBox.png"));
		window.type(songSearch, artist);
		return songSearch;
	}

	public void clickArtistSong() throws FindFailed {
		artistSongPlay = new Pattern(DataProperties.path("artistSelect.png"));
		window.wait(artistSongPlay);
		window.click(artistSongPlay);
	}

	public Pattern getSongPlayButtons() {
		songPlayButtons = new Pattern(DataProperties.path("songPlayButton.png"));
		return songPlayButtons;
	}

	public boolean verifySongPlaying() throws FindFailed {
		window.wait(songPlayButtons);
		window.click(songPlayButtons);
		boolean isSongPlaying = window.exists(songPauseButton) != null;
		return isSongPlaying;
	}

	public Pattern getPlayWindow() throws FindFailed {
		playWindow = new Pattern(DataProperties.path("playWindow.png"));
		window = getPage().wait(playWindow);
		return playWindow;
	}

	public void verifyPlayWindow() throws FindFailed {
		boolean exist = window.find(playWindow) != null;
		Assert.assertTrue(exist);
	}

}
