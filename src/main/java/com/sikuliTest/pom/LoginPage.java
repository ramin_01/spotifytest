package com.sikuliTest.pom;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.KeyModifier;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.sikuliTest.util.DataProperties;

public class LoginPage extends Page {

	private Pattern username;
	private Pattern password;
	private Pattern loginButton;
	private Pattern errorMessage;
	private Pattern loginWindow;
	private Region  window;
	private Screen screen;

	public LoginPage(Screen page) {
		super(page);
	}

	public Pattern getUsername(String userN) throws FindFailed {
		screen.click(username);
		screen.type(KeyModifier.CTRL, "aminrashedul01@gmail.com");
		screen.type(Key.BACKSPACE);
		username = new Pattern(DataProperties.path("loginUsername.png"));
		window.type(username, userN);
		return username;
	}

	public Pattern getPassword(String pass) throws FindFailed {
		password = new Pattern(DataProperties.path("loginPassword.png"));
		window.type(password, pass);
		return password;
	}

	public Pattern getLoginButton() {
		loginButton = new Pattern(DataProperties.path("loginButton.png"));
		return loginButton;
	}

	public void clickLoginButton() throws FindFailed {
		window.click(loginButton);
	}

	public Pattern getErrorMessage() {
		loginButton = new Pattern(DataProperties.path("failedLoginMessage.png"));
		return errorMessage;
	}

	public boolean verifyFailedMassege() throws FindFailed {
		boolean exist = window.find(errorMessage) != null;
		return exist;

	}

	public Pattern getLoginWindow() throws FindFailed {
		loginWindow = new Pattern(DataProperties.path("loginWindow.png"));
		window = getPage().wait(loginWindow);
		return loginWindow;
	}

	public void verifyLoginWindow() throws FindFailed {
		boolean exist = window.find(loginWindow) != null;
		Assert.assertTrue(exist);
	}

}
