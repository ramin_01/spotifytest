
package com.sikuliTest.spotify;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.sikuliTest.util.DataProperties;

public class LoginTest extends ScriptBase {
	
	

	@Test
	public void testValidLogin() throws Exception {
		spotifyApp().loginPage().verifyLoginWindow();
		spotifyApp().loginPage().getUsername(DataProperties.get("valid.login"));
		spotifyApp().loginPage().getPassword(DataProperties.get("valid.password"));
		spotifyApp().loginPage().clickLoginButton();
		Assert.assertFalse(spotifyApp().loginPage().verifyFailedMassege());
	}
	
	

	@Test
	public void testInvalidLogin() throws Exception {
		spotifyApp().loginPage().verifyLoginWindow();
		spotifyApp().loginPage().getUsername(DataProperties.get("invalid.login"));
		spotifyApp().loginPage().getPassword(DataProperties.get("invalid.password"));
		spotifyApp().loginPage().clickLoginButton();
		Assert.assertTrue(spotifyApp().loginPage().verifyFailedMassege());
	}
}
